Source: utf8gen
Maintainer: Debian QA Group <packages@qa.debian.org>
Section: text
Priority: optional
Build-Depends: debhelper-compat (=13), texinfo
Standards-Version: 4.6.0.1
Homepage: https://unifoundry.com/utf8gen/
Vcs-Browser: https://salsa.debian.org/debian/utf8gen
Vcs-Git: https://salsa.debian.org/debian/utf8gen.git
Rules-Requires-Root: no

Package: utf8gen
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, texinfo
Description: convert ASCII hexadecimal Unicode code points to UTF-8
 The utf8gen package contains one program, utf8gen, which reads
 hexadecimal numbers interpreted as Unicode code points and produces
 formatted output.  The numbers are provided one per line.  Each
 number is optionally followed by a space plus miscellaneous text.
 .
 The output is specified by providing printf(3)-style format strings
 on the command line.  This provides the flexibility to print UTF-8
 byte values in any desired base, echo the input value formatted as
 a comment for any desired programming language, or even to provide
 HTML-like table entries.  Optional miscellaneous text on each input
 line can be copied to the output.
